package com.skillion.cam_test_1;

import androidx.appcompat.app.AppCompatActivity;

import android.hardware.usb.UsbDevice;
import android.os.Bundle;
import android.util.Log;
import android.view.Surface;
import android.view.View;

import com.jiangdg.usbcamera.UVCCameraHelper;
import com.serenegiant.usb.widget.CameraViewInterface;

import butterknife.BindView;

public class MainActivity extends AppCompatActivity {
//    @BindView(R.id.camera_view)
//    public View mTextureView;

    CameraViewInterface mUVCCameraView;
    UVCCameraHelper mCameraHelper;

    private boolean isRequest;
    private boolean isPreview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        View mTextureView = findViewById(R.id.camera_view);

        mUVCCameraView = (CameraViewInterface) mTextureView;
        mUVCCameraView.setCallback(mCallback);
        mCameraHelper = UVCCameraHelper.getInstance();
        mCameraHelper.setDefaultFrameFormat(UVCCameraHelper.FRAME_FORMAT_MJPEG);
//        mCameraHelper.setDefaultPreviewSize(1280, 720);
        mCameraHelper.initUSBMonitor(this, mUVCCameraView, listener);




    }

    @Override
    protected void onStart() {

        super.onStart();
        if(mCameraHelper != null) {
            mCameraHelper.registerUSB();
        }
        if(mUVCCameraView != null) {
            mUVCCameraView.onResume();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(mCameraHelper != null) {
            mCameraHelper.unregisterUSB();
        }

        if(mUVCCameraView != null) {
            mUVCCameraView.onPause();
        }
    }

    private CameraViewInterface.Callback mCallback = new CameraViewInterface.Callback(){
        @Override
        public void onSurfaceCreated(CameraViewInterface view, Surface surface) {
            Log.d("*** ", "created");
            // must have
            if (!isPreview && mCameraHelper.isCameraOpened()) {
                mCameraHelper.startPreview(mUVCCameraView);
                isPreview = true;
            }
        }

        @Override
        public void onSurfaceChanged(CameraViewInterface view, Surface surface, int width, int height) {
            Log.d("*** ", "changed");
        }

        @Override
        public void onSurfaceDestroy(CameraViewInterface view, Surface surface) {
            Log.d("*** ", "destroyed");
            // must have
            if (isPreview && mCameraHelper.isCameraOpened()) {
                mCameraHelper.stopPreview();
                isPreview = false;
            }
        }
    };
    private UVCCameraHelper.OnMyDevConnectListener listener = new UVCCameraHelper.OnMyDevConnectListener() {

        @Override
        public void onAttachDev(UsbDevice device) {
            Log.d("*** ", "attached device");
            // request open permission(must have)
            if (!isRequest) {
                isRequest = true;
                if (mCameraHelper != null) {
                    mCameraHelper.requestPermission(0);
                }
            }
        }

        @Override
        public void onDettachDev(UsbDevice device) {
            Log.d("*** ", "detached device");
            // close camera(must have)
            if (isRequest) {
                isRequest = false;
                mCameraHelper.closeCamera();
            }
        }

        @Override
        public void onConnectDev(UsbDevice device, boolean isConnected) {
            Log.d("*** ", "connected device");
        }

        @Override
        public void onDisConnectDev(UsbDevice device) {
            Log.d("*** ", "disconnected device");
        }
    };
}